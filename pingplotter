#! /usr/bin/zsh
HELP="
 pingplotter — plots a nice live graph of ping times to a url in your terminal

 Usage:
   pingplotterdata [options]

 Options:
   -u <domain name>    pings this domain. Default is twitter.com
   -b <num>            backlog of pings to graph. X dimension of the graph
   -m <num>            max ping time display size, defaults to 200. 
                       Y dimension of the graph
   -h        Show this message.
"


# inspiration: https://www.grant-trebbin.com/2013/04/logging-and-graphing-ping-from-linux.html
# make sure you've installed gnuplot, otherthan that this should be portable
# see license and copyright info at the bottom
#
DATAFILE=/tmp/pingplotterdata$$.txt
DATAFILE_=/tmp/pingplotterdata2$$.txt

# make sure to clean up when we die
TRAPINT(){
  rm -f $DATAFILE
  rm -f $DATAFILE_
  return $1
}

help() {
  echo "$HELP"
  exit 0;
}
# echo "pingplotter [-h - help] [-u <domain name> - pings a domain. Default is twitter.com] [-b <num> backlog of pings to graph] [-m <num> - max ping size, defaults to 200]" >&2

# honestly, if my IP gets banned from pinging twitter I won't cry.
# I'll probably get more done.
URL=twitter.com
# 50 is a nice size and smooths out variablility in the graph for me
HISTORY=50
# my max ping is 200 usually. I'm fine with that.
MAXPING=200

# get options
while getopts 'hu:b:m:' c
do
  case $c in 
    h) help ;;
    u) URL=$OPTARG ;;
    b) HISTORY=$OPTARG ;;
    m) MAXPING=$OPTARG ;;
  esac
done

# prepopulate correct number of old zeros so graph X dimension doesn't scale
for (( a=0; a<$HISTORY; a++))
do
  echo "0" >> $DATAFILE
done

while true
do
  ping -c 1 $URL | grep from | awk -W interactive '{print $8}' | cut -c 6- >> $DATAFILE || continue
 # get last lines
 tail -$HISTORY $DATAFILE > $DATAFILE_ || continue
 echo "set terminal dumb size $COLUMNS, $LINES; set yrange [0:$MAXPING]; set xrange [0:$HISTORY]; plot '$DATAFILE_' title '$URL' with lines ls 1" | gnuplot || continue
 # overwrite original file
 mv $DATAFILE_ $DATAFILE || continue
done

# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
