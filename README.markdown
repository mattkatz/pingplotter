# Make a live ping plot of pings to a domain #

Get a realtime scrolling graph of ping time to a domain like this, but live:

    200 +-----------------------------------------------------------+
        |     +     +     +     +     +     +     +     +     +     |
        |                                     bluejeans.com ******* |
        |                                                           |
    150 |-+                                                       +-|
        |                                                           |
        |   *                                     *                 |
    100 |-+ *                                     *               +-|
        |  **                                     *         *      *|
        |  * *                                    **        *      *|
        | *  *                                   * *       * *    * |
     50 |**  * * ********    *** * **   **** *** * *   * * * * * **-|
        |     * *        ****   * *  ***    *   ** **** * *   * *   |
        |                                                           |
        |     +     +     +     +     +     +     +     +     +     |
      0 +-----------------------------------------------------------+
        0     5     10    15    20    25    30    35    40    45    50



## Installation

copy pingplotter to a place in your path and chmod u+x
Depends on [gnuplot](http://www.gnuplot.info/) and [zsh](https://www.zsh.org/), so make sure you've got those installed

## Usage
    pingplotter [-h - help] [-u <domain name> - pings a domain. Default is twitter.com] [-b <num> backlog of pings to graph] [-m <num> - max ping size, defaults to 200]

# Alternatives

I'm not the first to come up with this idea!

Consider: 

- pingplotter.com - not affiliated in any way, but what a full featured set of tools! Very heavy compared to this small script, but does tons more as well.
- gping - looks better, but requires a real installation. If you're already using homebrew, you might use this instead!
